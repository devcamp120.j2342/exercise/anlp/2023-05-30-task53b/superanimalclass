import models.Animal;
import models.Cat;
import models.Dog;
import models.Mammal;

public class App {
    public static void main(String[] args) throws Exception {
        Animal animal1 = new Animal("Do");
        Animal animal2 = new Animal("La");
        System.out.println(animal1);
        System.out.println(animal2);

        Mammal mammal1 = new Mammal("A");
        Mammal mammal2 = new Mammal("B");
        System.out.println(mammal1);
        System.out.println(mammal2);
        
        Cat cat1 = new Cat("Mi");
        Cat cat2 = new Cat("dog");
        System.out.println(cat1);
        System.out.println(cat2);

        Dog dog1 = new Dog("Ki");
        Dog dog2 = new Dog("cat");
        System.out.println(dog1);
        System.out.println(dog2);

        cat1.greets();
        cat2.greets();

        dog1.greets();
        dog2.greets();

        dog1.greets(dog2);
    }
}
